import unittest

from util.redis_connect import RedisPoolConnect


class RedisPoolConnectTest(unittest.TestCase):

    redis_pool_connect = None

    @classmethod
    def setUpClass(cls) -> None:
        cls.redis_pool_connect = RedisPoolConnect()

    def test_set_key_value(self):
        self.redis_pool_connect.set_key_value("test", "test")
        result = self.redis_pool_connect.find_key_value("test")
        self.assertTrue(result == "test")

    def test_find_key_value(self):
        result = self.redis_pool_connect.find_key_value("test")
        self.assertTrue(result == "test")


if __name__ == '__main__':
    unittest.main()