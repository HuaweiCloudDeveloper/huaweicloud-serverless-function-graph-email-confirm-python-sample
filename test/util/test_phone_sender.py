import unittest

from util.phone_sender import PhoneSender


class EmailSenderTest(unittest.TestCase):
    phone_sender = None

    @classmethod
    def setUpClass(cls) -> None:
        cls.phone_sender = PhoneSender()

    def test_send_msg(self):
        self.phone_sender.send_msg("", "['123456']")