import random
import unittest

from util.email_sender import EmailSender


class EmailSenderTest(unittest.TestCase):
    email_sender = None

    @classmethod
    def setUpClass(cls) -> None:
        cls.email_sender = EmailSender()

    def test_send_email(self):
        receiver = ""
        html = """<!DOCTYPE html>
                <html lang="en">
                <head>
                    <meta charset="UTF-8">
                    <title>Title</title>
                    <style type="text/css">
                       #box{
                            width: 300px;
                            height: 300px;
                            margin: 0 auto;
                        }
                       #box span{
                            display: block;
                            width: 200px;
                            height: 200px;
                            margin: 0 auto;
                       }
                    </style>
                </head>
                <body>
                <div id="box">
                <span>您的验证码是:test</span>
                </div>
                </body>
                </html>"""
        self.email_sender.send_email(receiver, html)


if __name__ == '__main__':
    unittest.main()
