import unittest

from util.kafka_producer_connect import KafkaProducerConnect


class KafkaProducerConnectTest(unittest.TestCase):

    kafka_producer_connect = None

    @classmethod
    def setUpClass(cls) -> None:
        cls.kafka_producer_connect = KafkaProducerConnect()

    def test_send_msg(self):
        import time
        # kafka发消息是异步非阻塞的，可能会因为在消息还没发出前，测试进程已结束，导致消息还没发出。所以需要进程休眠几秒。
        result = self.kafka_producer_connect.send_msg("test", {"test": "test"})
        time.sleep(3)
        self.assertTrue(result.succeeded)


if __name__ == '__main__':
    unittest.main()