import unittest

from util.config_center import ConfigCenter


class ConfigCenterTest(unittest.TestCase):

    def test_get_kafka_config(self):
        kafka_config = ConfigCenter.get_kafka_config()
        print(kafka_config)
        self.assertTrue(kafka_config)

    def test_get_redis_config(self):
        redis_config = ConfigCenter.get_redis_config(1)
        print(redis_config)
        self.assertTrue(redis_config)

    def test_get_email_sender_config(self):
        email_sender_config = ConfigCenter.get_email_sender_config()
        print(email_sender_config)
        self.assertTrue(email_sender_config)


if __name__ == '__main__':
    unittest.main()