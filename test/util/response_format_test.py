import unittest

from util.response_format import ResponseFormat


class ResponseFormatTest(unittest.TestCase):

    def test_to_json(self):
        rf = ResponseFormat({"msg": "test"}, 200)
        result = rf.to_json()
        print(result)
        self.assertTrue(result)

    def test_to_base64(self):
        rf = ResponseFormat({"msg": "test"}, 200, True)
        result = rf.to_base64()
        print(result)
        self.assertTrue(result)


if __name__ == '__main__':
    unittest.main()