import { createApp, onMounted } from 'vue'
import App from './App.vue'
import 'element-plus/dist/index.css'
import router from './router'
import './assets/main.css'
import Elementplus from 'element-plus'

const app = createApp(App)
app.use(router)
app.use(Elementplus)
app.mount('#app')
