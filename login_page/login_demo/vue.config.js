const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  lintOnSave:false,
  devServer: {
    proxy: {
      '/dev':{
        target: 'https://' + process.env.VUE_APP_DOMAIN,
        changeOrigin: true,
        secure: true,
        ws:true,
        pathRewrite:{
          '^/dev': ''
        }
      }
    }
  }
})
