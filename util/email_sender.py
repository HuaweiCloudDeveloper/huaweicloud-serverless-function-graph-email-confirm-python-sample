import smtplib
from email.mime.text import MIMEText
from email.header import Header

from util.config_center import ConfigCenter


class EmailSender:

    def __init__(self):
        self.config = ConfigCenter.get_email_sender_config()
        self._host = self.config.get("host")
        self._port = self.config.get("port")
        self._user = self.config.get("user")
        self._password = self.config.get("password")
        self._sender_address = self.config.get("sender_address")
        self._smtp_obj = smtplib.SMTP(self._host, self._port)
        self._smtp_obj.login(self._user, self._password)

    def send_email(self, receiver, message_html):
        """
        发送email
        :param receiver: 接收人
        :param message_html: 消息html
        :return:
        """
        message = MIMEText(message_html, 'html', 'utf-8')
        message['To'] = Header(receiver)
        message['Subject'] = Header("邮箱验证", 'utf-8')
        self._smtp_obj.sendmail(self._sender_address, receiver, message.as_string())
