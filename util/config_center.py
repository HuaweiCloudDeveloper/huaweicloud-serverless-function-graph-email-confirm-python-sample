import json
import os

# 环境变量名
KAFKA_CLUSTERS = "KAFKA_CLUSTERS"
REDIS_HOST = "REDIS_HOST"
REDIS_PORT = "REDIS_PORT"
REDIS_PASSWORD = "REDIS_PASSWORD"
EMAIL_HOST = "EMAIL_HOST"
EMAIL_PORT = "EMAIL_PORT"
EMAIL_USER = "EMAIL_USER"
EMAIL_PASSWORD = "EMAIL_PASSWORD"
EMAIL_SENDER_ADDRESS = "EMAIL_SENDER_ADDRESS"
PHONE_URL = "PHONE_URL"
PHONE_APP_KEY = "PHONE_APP_KEY"
PHONE_APP_SECRET = "PHONE_APP_SECRET"
PHONE_SENDER = "PHONE_SENDER"
PHONE_TEMPLATE_ID = "PHONE_TEMPLATE_ID"


class ConfigCenter:
    """配置中心"""
    @classmethod
    def get_value_from_env(cls, key):
        value = os.getenv(key)
        if not value:
            raise Exception("没有设置{0}环境变量".format(key))
        return value

    @classmethod
    def get_kafka_config(cls):
        return json.loads(cls.get_value_from_env(KAFKA_CLUSTERS))

    @classmethod
    def get_redis_config(cls, db_num):
        return {
            "host": cls.get_value_from_env(REDIS_HOST),
            "port": cls.get_value_from_env(REDIS_PORT),
            "password": cls.get_value_from_env(REDIS_PASSWORD),
            "db": db_num
        }

    @classmethod
    def get_email_sender_config(cls):
        return {
            "host": cls.get_value_from_env(EMAIL_HOST),
            "port": cls.get_value_from_env(EMAIL_PORT),
            "user": cls.get_value_from_env(EMAIL_USER),
            "password": cls.get_value_from_env(EMAIL_PASSWORD),
            "sender_address": cls.get_value_from_env(EMAIL_SENDER_ADDRESS),
        }

    @classmethod
    def get_phone_sender_config(cls):
        return {
            "url": cls.get_value_from_env(PHONE_URL),
            "app_key": cls.get_value_from_env(PHONE_APP_KEY),
            "app_secret": cls.get_value_from_env(PHONE_APP_SECRET),
            "sender": cls.get_value_from_env(PHONE_SENDER),
            "template_id": cls.get_value_from_env(PHONE_TEMPLATE_ID),
        }