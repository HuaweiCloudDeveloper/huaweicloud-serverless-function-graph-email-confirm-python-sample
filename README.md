# 华为云 FunctionGraph For Email/Phone Confirm项目演示

## 1. 说明

本项目将演示通过华为云FunctionGraph实现邮件/手机验证场景，让开发者上手体验FunctionGraph。

### 1.1 环境准备

* python3.9.2
* Pycharm 2022+

### 1.2 必备PyPI包

* kafka-python==2.0.2
* redis==4.3.4
* python-redis-lock==3.7.0
* jinja2==3.1.2

### 1.3 基础云服务能力

* 分布式缓存服务（Distributed Cache Service，简称DCS）更多详情，请参考 [分布式缓存服务 成长地图](https://support.huaweicloud.com/dcs/index.html)
* 分布式消息服务Kafka版 更多详情，请参考 [分布式消息服务Kafka版 成长地图](https://support.huaweicloud.com/kafka/index.html)
* 消息&短信（Message&SMS） 更多详情，请参考 [消息&短信 成长地图](https://support.huaweicloud.com/msgsms/index.html)
* NAT网关 更多详情，请参考 [NAT网关 成长地图](https://support.huaweicloud.com/natgateway/index.html)
* 对象存储服务（Object Storage Service，OBS）
  更多详情，请参考 [对象存储服务 OBS 成长地图](https://support.huaweicloud.com/obs/index.html)
* API网关（APIG）
  更多详情，请参考 [API网关 APIG 成长地图](https://support.huaweicloud.com/apig/index.html)  
### 1.4 核心代码介绍

* confirm_api.py 为验证码是否正确业务逻辑
* create_uuid.py 为创建邮件/手机验证任务业务逻辑
* email_verify.py 发送邮件验证业务逻辑
* phone_verify.py 发送手机短信验证业务逻辑
* email_template/templates/email.html 邮件模板
* util/config_center.py 为配置中心，负责加载环境变量中的配置信息
* util/kafka_producer_connect.py 为kafka的生产者实现，负责连接kafka发送消息
* util/redis_connect.py 为对redis的连接，实现对redis进行增查功能
* util/response_format.py 为响应的格式化，FunctionGraph配置APIG触发器响应格式是必须带有个别字段。
* util/phone_sender.py 手机信息发送逻辑
* util/email_sender.py 邮件发送逻辑

## 2.项目部署

### 2.1 本地必备程序

1. window10操作系统
2. 下载并安装[python3.9.2](https://www.python.org/downloads/release/python-392/) 。(
   注意安装后需要确认是否设置了python环境变量)
3. 下载并安装[Pycharm 2022+](https://www.jetbrains.com.cn/en-us/pycharm/)  
4. 下载并安装[node.js 16.17.0](https://nodejs.org/dist/v16.17.0/) 。(
   注意安装后需要确认是否设置了node.js环境变量)

### 2.2 基础环境搭建

1. 创建虚拟私有云 VPC ([快速入门](https://support.huaweicloud.com/qs-vpc/vpc_qs_0001.html))
2. 购买分布式缓存服务 DCS ([快速入门](https://support.huaweicloud.com/dcs/index.html))
3. 购买分布式消息服务Kafka版 ([快速入门](https://support.huaweicloud.com/kafka/index.html))
4. 购买API网关专享版([快速入门](https://support.huaweicloud.com/apig/index.html))
5. 开通消息&短信服务([快速入门](https://support.huaweicloud.com/qs-msgsms/sms_02_0001.html))
6. 购买NAT网关，并创建SNAT规则([快速入门](https://support.huaweicloud.com/qs-natgateway/zh-cn_topic_0087895790.html))

### 2.3 部署
1. 拉取代码  
   * git拉取代码，示例：
     >cd D://  
     git clone https://gitee.com/HuaweiCloudDeveloper/huaweicloud-serverless-function-graph-email-confirm-python-sample.git
   * 进入[代码仓](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-serverless-function-graph-email-confirm-python-sample) ，下载代码zip文件，解压zip文件到任意文件夹。  
     ![download_code_zip](./image4md/download_code_zip.PNG)  
2. 创建函数  
   步骤一 进入FunctionGraph页面  
   ![create_function01](./image4md/create_function01.PNG)  
   步骤二 创建函数  
   ![create_function02](./image4md/create_function02.PNG)  
   <br>
3. 创建委托  
   步骤一 进入函数页面-设置-权限。  
   ![create_agency01](./image4md/create_agency01.PNG)  
   步骤二 创建委托。  
   ![create_agency02](./image4md/create_agency02.PNG)  
   步骤三 可以根据函数所需云服务定制权限限制的权限，Tenant Administrator为除了iam用户权限所有云服务权限。函数主要用到服务有DMS和VPC的访问权限。  
   ![create_agency03](./image4md/create_agency03.PNG)  
   步骤四 选择已创建的委托，并点击保存。  
   ![create_agency04](./image4md/create_agency04.PNG)  
   <br>
4. 上传函数  
   步骤一  
   ![upload_function01](./image4md/upload_function01.PNG)  
   步骤二  
   ![upload_function02](./image4md/upload_function02.PNG)  
   步骤三 因文件命名差异，上传函数需要修改常规设置  
   ![upload_function03](./image4md/upload_function03.PNG)  
   注意：函数可能存在超时情况，建议执行超时时间可以默认的3秒往上增加数秒，防止超时导致函数执行失败。
   <br>
5. 设置VPC  
   步骤一 建议选择与其他服务实例（Kafka, redis, SNAT规则）相同的VPC及子网，并点击保存  
   ![set_vpc](./image4md/set_vpc.PNG)  
6. 设置环境变量  

* confirm_api.py  
  ![confirm_api_env_value](./image4md/confirm_api_env_value.PNG)  
* create_uuid.py  
  ![create_uuid_env_value](./image4md/create_uuid_env_value.PNG)  
* email_verify.py  
  ![email_verify_env_value](./image4md/email_verify_env_value.PNG)  
* phone_verify.py  
  ![phone_verify_env_value](./image4md/phone_verify_env_value.PNG)  
  注意：环境变量填写完，请点击保存。  
  <br>
  * KAFKA_CLUSTERS(图为：Kafka实例页面)  
  ![kafka_cluster](./image4md/kafka_cluster.PNG)  
  注1：KAFKA_CLUSTERS的值格式应为["ip:port", "ip:port"], 例["110.110.111.11:9092", "110.110.111.12:9092"]  
  <br>
  * REDIS_HOST、REDIS_PORT、REDIS_PASSWORD(图为：分布式缓存服务 Redis版实例页面)  
  ![redis](./image4md/redis.PNG)  
  注1：REDIS_PASSWORD为创建服务实例时设置密码  
  注2：这里的Redis服务是分布式缓存服务 Redis版  
  <br>
  * PHONE_URL、PHONE_APP_KEY、PHONE_APP_SECRET(图为：消息&短信服务实例页面)  
  ![phone_value01](./image4md/phone_value01.PNG)  
  PHONE_SENDER  
  ![phone_value02](./image4md/phone_value02.PNG)  
  PHONE_TEMPLATE_ID  
  ![phone_value03](./image4md/phone_value03.PNG)  
  <br>
  * 开启邮箱SMTP服务（以163邮箱为例），[163邮箱登录入口](https://email.163.com)  
  ![163_SMTP](./image4md/163_SMTP.PNG)  
  ![163_SMTP](./image4md/163_SMTP02.PNG)  
  EMAIL_PASSWORD  
  ![163_SMTP](./image4md/163_SMTP03.PNG)  
  EMAIL_HOST、EMAIL_PORT  
  ![163_SMTP](./image4md/163_SMTP04.PNG)  
  EMAIL_USERE、MAIL_SENDER_ADDRESS为账户的邮箱地址，例：********@163.com  
7. 绑定触发器  
   confirm_api及create_uuid绑定APIG专享版触发器  
   步骤一 创建触发器  
   ![create_trigger01](./image4md/create_trigger01.PNG)  
   步骤二 选择APIG专享版  
   ![create_trigger02](./image4md/create_trigger02.PNG)  
   步骤三 创建成功  
   ![create_trigger03](./image4md/create_trigger03.PNG)  
   <br>
   email_verify及phone_verify绑定分布式消息服务（Kafka）  
   步骤一 创建触发器  
   ![create_trigger01](./image4md/create_trigger01.PNG)  
   步骤二 选择分布式消息服务（Kafka）  
   ![create_trigger02_kafka](./image4md/create_trigger02_kafka.PNG)  
   步骤三 创建成功  
   ![create_trigger03_kafka](./image4md/create_trigger03_kafka.PNG)  
   注意：主题默认为email_topic和phone_topic  
8. 上传依赖/制作依赖  
   步骤一 制作依赖(本步骤可以选择跳过，已有制作好的依赖包直接上传，email_confirm\dependency.zip)  
   <br>
   环境准备  制作函数依赖包推荐在EulerOS环境中进行，原因函数执行的操作系统为EulerOS，使用其他系统打包可能会因为底层依赖库的原因，运行出问题，比如找不到动态链接库。  
   <br>
   编写requirements.txt文件。  
   > vi /tmp/requirements.txt  
   
   将所有需要的包写入其中，例：
   ```
   kafka-python==2.0.2
   redis==4.3.4
   requests==2.28.1
   jinja2==3.1.2
   ```
   安装第三方依赖包  
   > pip install /tmp/requirements.txt --target=/tmp/dependency -i https://repo.huaweicloud.com/repository/pypi/simple   
   
   注意：可能会出现个别包版本不存在的情况，可以尝试更改-i参数后面的镜像源地址  
   <br>
   切换到依赖包的安装目录  
   > cd /tmp/dependency  
   
   打包所有依赖到zip文件（zip找不到，下载并安装zip）  
   > zip -rq dependency.zip *  
   
   下载dependency.zip到本地  
   
   步骤二 上传依赖  
   ![create_dependency01](./image4md/create_dependency01.PNG)   
   ![create_dependency02](./image4md/create_dependency02.PNG)   
   函数页面下方，代码依赖包
   ![create_dependency03](./image4md/create_dependency03.PNG)   
   ![create_dependency04](./image4md/create_dependency04.PNG)   
9. API绑定应用  
   步骤一 进入APIG界面,点击进入专享版控制台  
   [API网关](https://console.huaweicloud.com/apig)  
   ![apig_test01](./image4md/apig_test01.PNG)  
   步骤二 创建应用  
   ![apig_test02](./image4md/apig_test02.PNG)  
   步骤三 绑定API    
   ![apig_test03](./image4md/apig_test03.PNG)  
   步骤四 选定已创建API   
   ![apig_test04](./image4md/apig_test04.PNG)  
   
### 2.4 测试

1. 本地windows系统部署测试  
   步骤一 修改 项目根路径/login_page/login_demo/.env.development文件  
   ![local_test01](./image4md/local_test01.PNG)  
   > VUE_APP_KEY值为API绑定应用的App_Key  
   > VUE_APP_SECRET值为API绑定应用的App_Secret  
   > ![local_test05](./image4md/local_test05.PNG)  
   > VUE_APP_DOMAIN(APIG默认分配一个未备案的子域名，如果已有备案域名可以和[APIG分组进行绑定](https://support.huaweicloud.com/usermanual-apig/apig_03_0006.html))  
   > ![local_test02](./image4md/local_test02.PNG)  
   > 注意：因子域名未经过备案，所以DNS无法解析该域名，需要修改本地hosts文件绑定域名和公网IP，具体步骤见2.5常见问题3.windows。  
   >
   步骤二 打开终端，切换路径至login_page/login_demo（以下操作Pycharm为例） 
   ![local_test03](./image4md/local_test03.PNG)  
   ```
   cd .\login_page\login_demo\  
   ```
   步骤三 下载vue.js第三方依赖  
   ```
   npm install  
   ```
   注意：报错找不到命令npm，可能原因为环境变量还未设置或尝试重启Pycharm。  
   步骤四 启动前端服务  
   ```
   npm run serve  
   ```
   步骤五 Chrome浏览器打开http://localhost:8080  
   ![local_test06](./image4md/local_test06.PNG)  
   ![local_test07](./image4md/local_test07.PNG)  
   
2. ECS EulerOS操作系统部署测试  
   步骤一 购买ECS服务器并绑定弹性公网IP  
   ![ECS_test01](./image4md/ECS_test01.PNG)  
   ![ECS_test02](./image4md/ECS_test02.PNG)  
   ![ECS_test03](./image4md/ECS_test03.PNG)  
   ![ECS_test04](./image4md/ECS_test04.PNG)  
   下载并安装[MobaXterm](https://mobaxterm.mobatek.net/download.html)  
   ![ECS_test05](./image4md/ECS_test05.PNG)  
   连接弹性服务器  
   ![ECS_test06](./image4md/ECS_test06.PNG)  
   ![ECS_test07](./image4md/ECS_test07.PNG)  
   ![ECS_test08](./image4md/ECS_test08.PNG)  
   ![ECS_test09](./image4md/ECS_test09.PNG)  
   步骤二 修改 项目根路径/login_page/login_demo/.env.production文件  
   ![ECS_test14](./image4md/ECS_test14.PNG)  
   > VUE_APP_KEY值为API绑定应用的App_Key  
   > VUE_APP_SECRET值为API绑定应用的App_Secret  
   > ![local_test05](./image4md/local_test05.PNG)  
   > VUE_APP_DOMAIN(APIG默认分配一个未备案的子域名，如果已有备案域名可以和[APIG分组进行绑定](https://support.huaweicloud.com/usermanual-apig/apig_03_0006.html))  
   > ![local_test02](./image4md/local_test02.PNG)  
   > 注意：因子域名未经过备案，所以DNS无法解析该域名，需要修改本地hosts文件绑定域名和公网IP，具体步骤见2.5常见问题3.linux。 
   >
   步骤三 构建前端代码上传至ECS服务器  
   通过Pycharm打开项目，并打开终端和执行切换目录命令  
   ```
   cd .\login_page\login_demo\  
   ```
   ![local_test03](./image4md/local_test03.PNG)  
   下载vue.js第三方依赖(已下载可以跳过)  
   ```
   npm install
   ```
   ![ECS_test10](./image4md/ECS_test10.PNG)  
   构建前端代码  
   ```
   npm run build
   ```
   ![ECS_test11](./image4md/ECS_test11.PNG)  
   ![ECS_test12](./image4md/ECS_test12.PNG)  
   ![ECS_test13](./image4md/ECS_test13.PNG)  
   步骤四 命令行安装Nginx  
   ```
   yum install nginx  
   ```
   步骤五 修改Nginx配置文件并启动nginx  
   ```
   vim /etc/nginx/nginx.conf
   ```
   ![nginx_conf](./image4md/nginx_conf.PNG)  
   注意：root 如图所示填前端构建dist文件夹路径。如果涉及跨域需要修改location部分， proxy_pass为后端服务地址，可使用自动生成的子域名。具体配置可参考项目下nginx.conf文件。   
   注意：因子域名未经过备案，所以DNS无法解析该域名，需要修改本地hosts文件绑定域名和公网IP，具体步骤见2.5常见问题3.linux。  
   步骤五 访问服务  
   打开Chrome浏览器，访问服务器公网IP地址  
3. OBS + DNS部署测试  
   > 前提条件  
   > 1. 拥有已备案通过的公网域名，详见[域名注册](https://support.huaweicloud.com/domain/index.html)  
   > 
   步骤一 APIG实例分组绑定域名  
   选择函数所在分组  
   ![APIG_Domain_01](./image4md/APIG_Domain_01.PNG)  
   单击分组信息及绑定独立域名  
   ![APIG_Domain_02](./image4md/APIG_Domain_02.PNG)  
   输入子域名  
   ![APIG_Domain_03](./image4md/APIG_Domain_03.PNG)  
   步骤二 APIG实例分组添加跨域预校验OPTIONS方法  
   单击创建API  
   ![create_options_01](./image4md/create_options_01.PNG)  
   选择配置项  
   ![create_options_02](./image4md/create_options_02.PNG)  
   ![create_options_03](./image4md/create_options_03.PNG)  
   > Access-Control-Allow-Origin表示允许跨域的源  
   > Access-Control-Allow-Credentials表示是否允许带上cookies  
   > Access-Control-Allow-Methods允许访问的方法  
   > Access-Control-Allow-Headers允许请求带上的头信息  
   > 
   步骤三 修改前端代码生产环境配置  
   修改配置文件，项目路径/login_page/login_demo/.env.production  
   ![pro_config](./image4md/pro_config.PNG)  
   > VUE_APP_BASE_URL为http://test.example.com(APIG绑定域名)  
   > VUE_APP_DOMAIN为test.example.com(APIG绑定域名)  
   > 
   步骤四 构建前端代码  
   通过Pycharm打开项目，并打开终端和执行切换目录命令  
   ```
   cd .\login_page\login_demo\
   ```
   ![local_test03](./image4md/local_test03.PNG)  
   下载vue.js第三方依赖(已下载可以跳过)  
   ```
   npm install
   ```
   ![ECS_test10](./image4md/ECS_test10.PNG)  
   构建前端代码  
   ```
   npm run build
   ```
   ![ECS_test11](./image4md/ECS_test11.PNG)  
   ![ECS_test12](./image4md/ECS_test12.PNG)  
   步骤五 通过[OBS Browser+](https://support.huaweicloud.com/intl/zh-cn/browsertg-obs/obs_03_1003.html) 上传前端代码  
   ![upload_front_code](./image4md/upload_front_code.PNG)  
   注意：建议按照图中文件路径上传文件  
   步骤六 配置静态页面托管  
   ![obs_config_01](./image4md/obs_config_01.PNG)  
   步骤七 配置自定义域名  
   ![obs_config_02](./image4md/obs_config_02.PNG)  
   步骤八 DNS添加记录集  
   进入华为云DNS控制台，单击域名注册  
   ![DNS_01](./image4md/DNS_01.PNG)  
   选择已备案域名，单击解析  
   ![DNS_02](./image4md/DNS_02.PNG)  
   添加前端静态页面记录集（CNAME类型）  
   ![DNS_front](./image4md/DNS_front.PNG)  
   值为OBS托管静态页面域名，如下图：  
   ![OBS_domain](./image4md/OBS_domain.PNG)  
   添加APIG记录集（A类型）  
   ![DNS_APIG](./image4md/DNS_APIG.PNG)  
   值为APIG入口IP地址，如下图：  
   ![APIG_IP](./image4md/APIG_IP.PNG)  
   步骤九 测试访问  
   打开浏览器并访问前端域名  
   
### 2.5 常见问题

1. 实例需要打开对应的端口安全组入方向规则，让各服务之间可以互相访问。  

2. 导致各数据库或消息队列实例连接超时原因，一般为安全组入方向规则没有打开或者打开错误。  
   
3. 未备案域名，服务器配置IP映射域名。  
> * windows  
>> 修改 C:\WINDOWS\system32\drivers\etc\hosts 文件。  
>> ![](./image4md/windows_hosts.PNG)  
>> 打开cmd，测试域名是否能访问。  
>> ```
>> ping 域名
>> ```
>> ![](./image4md/windows_ping.PNG)  
> * linux  
>> 修改 /etc/hosts 文件。 
>> ```
>> vim /etc/hosts
>> ```
>> ![](./image4md/linux_hosts.PNG)  
>> ```
>> ping 域名  
>> ```
>> ![](./image4md/linux_ping.PNG)  
> 
> 注意：域名是网址的组成部分，例如http://localhost.com/test 中localhost.com为域名  
> 